<!DOCTYPE html>
<?php include 'header.php';?>

    <script type="module" src="scripts/f1_previous_seasons_api.js"></script>

    <div class="content col-md-12 height-overflow-fix">
        <h1>Previous Seasons</h1>
        <h3>2022</h3>
        <div id="pst0" class="table_container mb-4"></div>
        <h3>2021</h3>
        <div id="pst1" class="table_container mb-4"></div>
        <h3>2020</h3>
        <div id="pst2" class="table_container mb-4"></div>
    </div>

<?php include 'footer.php';?>