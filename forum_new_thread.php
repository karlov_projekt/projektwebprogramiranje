<?php
include 'header.php';
?>

<div class="container">
    <div class="d-flex justify-content-center height-overflow-fix">
        <div class="card">
            <div class="card-header">
                <h2>Add new Thread</h2>
            </div>
            <div class="card-body">
                <form method="post" action="" class="d-flex flex-column">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text justify-content-center"><i class="fas fa-bars"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Thread name" name="thread_name" id="thread_name">
                    </div>

                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text justify-content-center"><i class="fas fa-comment-dots"></i></span>
                        </div>
                        <textarea type="text" class="form-control" placeholder="Thread Description" name="thread_description" id="thread_description" rows="5"></textarea>
                    </div>
                
                    <div class="form-group mt-3">
                        <input class="btn primary_btn float-left" type="button" value="GO BACK" onclick="window.location.href='forum.php'">

                        <input id="addNewThreadBtn" type="submit" value="ADD Thread" class="btn float-right primary_btn">
                    </div>
                </form>

                <div class="message_box" style="margin:10px 0px;"></div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    const delay = 2000;

    $('#addNewThreadBtn').click((e) => {
        e.preventDefault();

        var name = $('#thread_name').val();
        if(name == ''){
            $('.message_box').html(
                '<span style="color:red;">Enter thread title!</span>'
            );
            $('#thread_name').focus();
            return false;
        }
                
        var desc = $('#thread_description').val();
        if(desc == ''){
            $('.message_box').html(
                '<span style="color:red;">Enter the description!</span>'
            );
            $('#thread_description').focus();
            return false;
        }
             
        $.ajax(
            {
                type: "POST",
                url: "add_new_thread.php",
                data: "thread_name="+name+"&thread_description="+desc,
                beforeSend: () => {
                    $('.message_box').html(
                        '<img src="assets/loader.gif" width="25" height="25"/>'
                    );
                },		 
                success: (data) => {
                    if (data.includes('failed')) {
                        setTimeout(function() {
                            $('.message_box').html(`<span style="color:red;">${data}</span>`);
                        }, delay);
                    } else {
                        window.location = `forum_thread.php?thread_id=${data}`
                    }
                }
            }
        );
    });
			
});
</script>

<?php include 'footer.php';?>