<!DOCTYPE html>
<?php
  include 'connection.php';
  include 'sign_up_header.php';
?>
  <div class="container">
          <div class="d-flex justify-content-center h-100">
            <div class="card">
              <div class="card-header">
                <h2>Register</h2>
                <img src="assets\formula1-logo.png" class="logo">
              </div>
              <div class="card-body">
              <form action="" method="post">
              <div class="input-group form-group">
                <div class="input-group-prepend">
                  <span class="input-group-text justify-content-center"><i class="fas fa-envelope"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Email" name="email" id="email">
              </div>
              <div class="input-group form-group">
                <div class="input-group-prepend">
                  <span class="input-group-text justify-content-center"><i class="fas fa-user"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Username" name="username" id="username">
              </div>
              <div class="input-group form-group">
                <div class="input-group-prepend">
                  <span class="input-group-text justify-content-center"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" class="form-control" placeholder="Password" name="password" id="password">
              </div>
              <div class="input-group form-group">
                <div class="input-group-prepend">
                  <span class="input-group-text justify-content-center"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" class="form-control" placeholder="Repeat your password" name="password_repeat" id="repeat_password">
              </div>
              <div class="form-group">
                <input id = "registerUserBtn" type="submit" value="Register" class="btn float-right primary_btn" name="Submit">
              </div>
            </form>
            <div class="message_box" style="margin:10px 0px;"></div>
          
   </div>
            <div class="card-footer">
              <div class="d-flex justify-content-center links">
                Are you an existing user?<a href="login.php">Login</a>
              </div>
            </div>
          </div>
          <div class="d-flex justify-content-end social_icon" id="font-awesome-colors">
            <span><a href="https://www.facebook.com/Formula1/"><i class="fab fa-facebook-square"></i></a></span>
            <span><a href="https://twitter.com/f1"><i class="fab fa-twitter-square"></i></a></span>
            <span><a href="https://www.youtube.com/channel/UCB_qr75-ydFVKSF9Dmo6izg"><i
                  class="fab fa-youtube-square"></i></a></span>
          </div>
        </div>
      </div>
      </body>
    </html>

<script>

  $(document).ready(function() {
      const delay = 2000;

      $('#registerUserBtn').click((e) => {
          e.preventDefault();

          var email = $('#email').val();
          if(email == ''){
              $('.message_box').html(
                  '<span style="color:red;">Enter your email!</span>'
              );
              $('#email').focus();
              return false;
          }
                  
          var name = $('#username').val();
          if(name == ''){
              $('.message_box').html(
                  '<span style="color:red;">Enter your username!</span>'
              );
              $('#username').focus();
              return false;
          }

          var password = $('#password').val();
          if(password == ''){
              $('.message_box').html(
                  '<span style="color:red;">Enter your password!</span>'
              );
              $('#password').focus();
              return false;
          }

          var repeat_password = $('#repeat_password').val();
          if(repeat_password == ''){
              $('.message_box').html(
                  '<span style="color:red;">Enter your password again!</span>'
              );
              $('#repeat_password').focus();
              return false;
          }

          if(password != repeat_password){
            $('.message_box').html(
                  '<span style="color:red;">Enter the same password!</span>'
              );
            $('#repeat_password').focus();
            return false;
          }
              
          $.ajax(
              {
                  type: "POST",
                  url: "add_new_user.php",
                  data: "email=" + email + "&username=" + name + "&password=" + password,
                  beforeSend: () => {
                      $('.message_box').html(
                          '<img src="assets/loader.gif" width="25" height="25"/>'
                      );
                  },		 
                  success: (data) => {
                      if (data.includes('already')) {
                          setTimeout(function() {
                              $('.message_box').html(`<span style="color:red;">${data}</span>`);
                          }, delay);
                      } else {
                        window.location = "login.php"
                         
                      }
                  }
              }
          );
      });
        
  });
</script>