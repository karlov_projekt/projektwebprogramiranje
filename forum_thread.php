<!DOCTYPE html>
<?php 
include 'header.php';
include 'connection.php';
?>

    <div class="text-center col-md-12 height-overflow-fix">
        <h1>Thread</h1>
        <?php 
            session_start();
            $id = $_GET['thread_id'];
            echo "<script>var thread_id = '$id';</script>";

            $sql="SELECT * FROM threads where thread_id='$id'";
            $data = mysqli_query($conn, $sql);
            $row = mysqli_fetch_array($data);
            
            $sql = "SELECT * FROM posts where thread_id = '$id'";
            if($sql != 0){
                $data = mysqli_query($conn, $sql);
                $row = mysqli_fetch_array($data);
                
                if($row != 0){
                    echo $row['post_description'];  
                } else {

                }
               
            }
            
        ?>
        <div class="table_container">
            <table class="tg">
                <tbody>
                    <?php 
                        if (isset($_GET['page_no']) && $_GET['page_no']!="") {
                            $page_no = $_GET['page_no'];
                        } else {
                            $page_no = 1;
                        }

                        $total_records_per_page = 10;

                        $offset = ($page_no-1) * $total_records_per_page;
                        $previous_page = $page_no - 1;
                        $next_page = $page_no + 1;
                        $adjacents = "2";

                        $result_count = mysqli_query(
                            $conn,
                            "SELECT COUNT(*) As total_records FROM `posts`"
                            );

                        $total_records = mysqli_fetch_array($result_count);
                        $total_records = $total_records['total_records'];
                        $total_no_of_pages = ceil($total_records / $total_records_per_page);
                        $second_last = $total_no_of_pages - 1; // total pages minus 1

                        $result = mysqli_query(
                            $conn,
                            "SELECT * FROM `posts` LIMIT $offset, $total_records_per_page"
                            );
                        while($row = mysqli_fetch_array($result)) {
                            echo '<tr>
                                        <td>
                                            <a href="forum_thread.php?thread_id='.$row['thread_id'].'">'.$row['post_description'].'</a>
                                        </td>
                                        <td>'.$row['author_username'].'</td>
                                    </tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="container">
            <div class="d-flex justify-content-center">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="" class="d-flex flex-column">
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text justify-content-center"><i class="fas fa-comment-dots"></i></span>
                                </div>
                                <textarea type="text" class="form-control" placeholder="Quick reply" name="post_description" id="post_description" rows="5"></textarea>
                            </div>
                        
                            <div class="form-group mt-3">
        
                                <input id="addNewPostBtn" type="submit" value="Post" class="btn float-right primary_btn">
                            </div>
                        </form>
        
                        <div class="message_box" style="margin:10px 0px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
$(document).ready(function() {
    const delay = 2000;

    $('#addNewPostBtn').click((e) => {
        e.preventDefault();
                
        var desc = $('#post_description').val();
        if(desc == ''){
            $('.message_box').html(
                '<span style="color:red;">Enter the description!</span>'
            );
            $('#thread_description').focus();
            return false;
        }
             
        $.ajax(
            {
                type: "POST",
                url: "add_new_post.php",
                data: "thread_id=" + thread_id + "&post_description=" + desc,
                beforeSend: () => {
                    $('.message_box').html(
                        '<img src="assets/loader.gif" width="25" height="25"/>'
                    );
                },		 
                success: (data) => {
                    if (data.includes('failed')) {
                        setTimeout(function() {
                            $('.message_box').html(data);
                        }, delay);
                    } else {
                        window.location = `forum_thread.php?thread_id=${thread_id}`
                    }
                }
            }
        );
    });
			
});
</script>

<?php include 'footer.php';?>


