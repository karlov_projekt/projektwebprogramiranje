<!DOCTYPE html>
<?php include 'header.php';?>
    <script type="module" src="scripts/f1_drivers_api.js"></script>

    <div class="content col-md-12 height-overflow-fix pb-4">
        <h1>Driver Standings</h1>
        <div id="driversTable" class="table_container"></div>
    </div>

<?php include 'footer.php';?>