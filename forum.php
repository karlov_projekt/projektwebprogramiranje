<?php
    include 'connection.php';
    include 'header.php';
?>
    <h1 class="text-center">F1 Forum</h1>

    <div class="table-container col-md-12">
        <h3>Talk about F1 related topics. Enjoy!</h3>
        <div class="table_container">
            <table class="tg">
                <thead>
                    <tr>
                        <th style='width:19rem;'>Thread name</th>
                        <th style='width:10rem;'>Author</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        if (isset($_GET['page_no']) && $_GET['page_no']!="") {
                            $page_no = $_GET['page_no'];
                        } else {
                            $page_no = 1;
                        }

                        $total_records_per_page = 10;

                        $offset = ($page_no-1) * $total_records_per_page;
                        $previous_page = $page_no - 1;
                        $next_page = $page_no + 1;
                        $adjacents = "2";

                        $result_count = mysqli_query(
                            $conn,
                            "SELECT COUNT(*) As total_records FROM `threads`"
                            );
                        $total_records = mysqli_fetch_array($result_count);
                        $total_records = $total_records['total_records'];
                        $total_no_of_pages = ceil($total_records / $total_records_per_page);
                        $second_last = $total_no_of_pages - 1; // total pages minus 1

                        $result = mysqli_query(
                            $conn,
                            "SELECT * FROM `threads` LIMIT $offset, $total_records_per_page"
                            );
                        while($row = mysqli_fetch_array($result)) {
                            echo '<tr>
                                        <td>
                                            <a href="forum_thread.php?thread_id='.$row['thread_id'].'">'.$row['thread_name'].'</a>
                                        </td>
                                        <td>'.$row['author_username'].'</td>
                                    </tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>

        <div class="mt-2 mb-3">
            <strong>Page <?php echo $page_no." of ".$total_no_of_pages; ?></strong>
        </div>

        <ul class="pagination justify-content-center">
            <?php if($page_no > 1){
                    echo "<li class='page-item'><a class='page-link' href='?page_no=1'>First Page</a></li>";
                } 
            ?>
                
            <li  class='page-item <?php if($page_no <= 1){ echo "disabled"; } ?>' >
                <a class='page-link' <?php if($page_no > 1){
                    echo "href='?page_no=$previous_page'";
                } ?>>Previous</a>
            </li>
                
            <li class='page-item <?php if($page_no >= $total_no_of_pages){ echo "disabled"; } ?>' >
                <a class='page-link' <?php if($page_no < $total_no_of_pages) {
                    echo "href='?page_no=$next_page'";
                } ?>>Next</a>
            </li>

            <?php if($page_no < $total_no_of_pages){
                echo "<li class='page-item'><a class='page-link' href='?page_no=$total_no_of_pages'>Last &rsaquo;&rsaquo;</a></li>";
            } 
            ?>
        </ul>

    </div>

    <a class="btn primary_btn" href="forum_new_thread.php">
        <span>Add New Thread</span>
    </a>

<?php include 'footer.php';?>