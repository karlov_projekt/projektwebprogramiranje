<!DOCTYPE html>
<?php include 'header.php';?>
    <script type="module" src="scripts/f1_teams_api.js"></script>

    <div class="content col-md-12">
        <h1>Team Standings</h1>
        <div id="standingsTable" class="table_container"></div>
    </div>

<?php include 'footer.php';?>