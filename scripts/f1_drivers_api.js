var drivers = [];
window.onload = function () {
  const options = {
    method: "GET",
    headers: {
        'X-RapidAPI-Key': 'e55d7206ccmshe56d83d284035dbp1a7030jsn2b8963ebd0e6',
        'X-RapidAPI-Host': 'fia-formula-1-championship-statistics.p.rapidapi.com'
      },
  };
  
  fetch(
    "https://fia-formula-1-championship-statistics.p.rapidapi.com/api/drivers",
    options
    )
    .then((response) => response.json())
    .then((response) => {
      drivers = response;
      fillDriversTable();
    })
    .catch((err) => console.error(err));
  };
  
  function fillDriversTable() {
    //Create a HTML Table element.
    var table = document.createElement("table");
    table.classList = ["tg"];
    table.border = "1";
    
    //Add the header row.
  var row = table.insertRow(-1);
  
  //Add the header cells.
  var headerCell = document.createElement("TH");
  headerCell.innerHTML = "Rank";
  row.appendChild(headerCell);
  
  headerCell = document.createElement("TH");
  headerCell.innerHTML = "Points";
  row.appendChild(headerCell);
  
  headerCell = document.createElement("TH");
  headerCell.innerHTML = "Firstname";
  row.appendChild(headerCell);

  headerCell = document.createElement("TH");
  headerCell.innerHTML = "Lastname";
  row.appendChild(headerCell);

  
  
  //Add the data rows.
  for (var i = 0; i < 22; i++) {
    //Add the data row.
    var row = table.insertRow(-1);
    row.id = i;
    
    //Add the data cells.
    
    cell = row.insertCell(-1);
    cell.innerHTML = drivers.drivers[i].rank;

    cell = row.insertCell(-1);
    cell.innerHTML = drivers.drivers[i].points;
    
    cell = row.insertCell(-1);
    cell.innerHTML = drivers.drivers[i].firstname;
    
    var cell = row.insertCell(-1);
    cell.innerHTML = drivers.drivers[i].lastname;

    
    
    //
  }
  
  var dvTable = document.getElementById("driversTable");
  dvTable.innerHTML = "";
  dvTable.appendChild(table);
}
