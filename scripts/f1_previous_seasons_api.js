import { fetchData } from "./fetch.js";

const columns = ['Position', 'Team', 'Points'];
const endpoints = [
  'https://fia-formula-1-championship-statistics.p.rapidapi.com/api/standings/constructors-standings?year=2022',
  'https://fia-formula-1-championship-statistics.p.rapidapi.com/api/standings/constructors-standings?year=2021',
  'https://fia-formula-1-championship-statistics.p.rapidapi.com/api/standings/constructors-standings?year=2020',
];

window.onload = async function () {
  console.log('doin')
  var data = await fetchData(endpoints[0], columns);
  fillPSTable(data.table, data.response, 0);

  data = await fetchData(endpoints[1], columns);
  fillPSTable(data.table, data.response, 1);

  data = await fetchData(endpoints[2], columns);
  fillPSTable(data.table, data.response, 2);
};
  
function fillPSTable(table, response, index) {
//Add the data rows.
for (var i = 0; i < 10; i++) {
  //Add the data row.
  var row = table.insertRow(-1);
  row.id = i;
  
  var cell = row.insertCell(-1);
  cell.innerHTML = response.constructorStandings[i].pos;

  cell = row.insertCell(-1);
  cell.innerHTML = response.constructorStandings[i].team;
  
  cell = row.insertCell(-1);
  cell.innerHTML = response.constructorStandings[i].pts;
}

var dvTable = document.getElementById(`pst${index}`);
dvTable.innerHTML = "";
dvTable.appendChild(table);
}