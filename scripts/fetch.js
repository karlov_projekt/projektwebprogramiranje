
export async function fetchData(endpoint, columns) {
  const options = {
    method: "GET",
    headers: {
      'X-RapidAPI-Key': 'c9dbf2ab4emsh0fcdc4a8d7b59bcp11f246jsn040a4cf252fd',
    'X-RapidAPI-Host': 'fia-formula-1-championship-statistics.p.rapidapi.com',
    },
  };

  let data = await fetch(endpoint,options)
    .then((response) => response.json())
    .then((response) => prepareTable(response, columns))
    .catch((err) => console.error(err));
    return data;
  };
    
function prepareTable(response, columns) {
  //Create a HTML Table element.
  var table = document.createElement("table");
  table.classList = ["tg"];
  table.border = "1";
    
    //Add the header row.
  var row = table.insertRow(-1);
  
  //Add the header cells.
  columns.forEach(col => {
    var headerCell = document.createElement("TH");
    headerCell.innerHTML = col;
    row.appendChild(headerCell);
  });
  
  return { table: table, response: response};
}