import { fetchData } from "./fetch.js";

var columns = ['Team', 'Drivers', 'Ranking', 'Points'];
var endpoint = "https://fia-formula-1-championship-statistics.p.rapidapi.com/api/teams";

window.onload = async function () {
  const data = await fetchData(endpoint, columns);
  fillStandingsTable(data.table, data.response);
};
  
function fillStandingsTable(table, response) {
  //Add the data rows.
  for (var i = 0; i < response.teams.length; i++) {
    //Add the data row.
    var row = table.insertRow(-1);
    row.id = i;
    
    //Add the data cells.
    var cell = row.insertCell(-1);
    cell.innerHTML = response.teams[i]["teamName"];
    
    cell = row.insertCell(-1);
    cell.innerHTML =
      response.teams[i].drivers[0]["lastname"] +
      ", " +
      response.teams[i].drivers[1]["lastname"];
    
    cell = row.insertCell(-1);
    cell.innerHTML = response.teams[i].rank["standing"];
    
    cell = row.insertCell(-1);
    cell.innerHTML = response.teams[i].points["pts"];
  }
  
  var dvTable = document.getElementById("standingsTable");
  dvTable.innerHTML = "";
  dvTable.appendChild(table);
}