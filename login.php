<!DOCTYPE html>
<?php
  include 'connection.php';
  include 'sign_up_header.php';

  echo '<div class="container">
    <div class="d-flex justify-content-center h-100">
       <div class="card">
         <div class="card-header">
           <h2>Sign In</h2>
           <img src="assets\formula1-logo.png" class="logo">
         </div>
        <div class="card-body"> ';

  if(isset($_SESSION['signed_in']) && $_SESSION['signed_in'] == true) {
    echo 'You are already signed in, you can <a href="signout.php">sign out</a> if you want.';
  } else {
    if($_SERVER['REQUEST_METHOD'] != 'POST') {
      echo '<form action="#" method="post">
              <div class="input-group form-group">
                <div class="input-group-prepend">
                  <span class="input-group-text justify-content-center"><i class="fas fa-user"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Username" name="username" id="username">
              </div>

              <div class="input-group form-group">
                <div class="input-group-prepend">
                  <span class="input-group-text justify-content-center"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" class="form-control" placeholder="Password" name="password" id="password">
              </div>
              
              <div class="form-group">
                <input type="submit" name="Submit" class="btn float-right primary_btn">
              </div>
            </form>
          ';
    } else {
      $errors = array();
      if(!isset($_POST['username'])) {
        $errors[] = 'The username field must not be empty.';
      }
      if(!isset($_POST['password'])) {
        $errors[] = 'The password field must not be empty.';
      }
      if(!empty($errors)) {
        echo 'Uh-oh.. a couple of fields are not filled in correctly..';
        echo '<ul>';
        foreach($errors as $key => $value) {
          echo '<li>' . $value . '</li>'; 
        }
        echo '</ul>';
      } else {
        $sql = "SELECT Username, Password FROM users WHERE Username = '" . $_POST['username'] . "' 
          AND Password = '" . $_POST['password'] . "'";
        $result = mysqli_query($conn, $sql);
        if(!$result) {
          echo 'Something went wrong while signing in. Please try again later.';
        } else {
          if(mysqli_num_rows($result) == 0) {
            echo 'You have supplied a wrong user/password combination. Please try again.';
          } else {
            session_start();
            $_SESSION['signed_in'] = true;
            
            while($row = mysqli_fetch_assoc($result)){
              $_SESSION['Username'] = $row['Username'];
            }
            header( "Location: home.php" );
          }
        }
      }
    }
  }

  echo '</div>
  <div class="card-footer">
      <div class="d-flex justify-content-center links">
        <span>Need a new account?</span>
        <a href="register.php">Register</a>
      </div>
    </div>
  </div>
  <div class="d-flex justify-content-end social_icon" id="font-awesome-colors">
    <span><a href="https://www.facebook.com/Formula1/"><i class="fab fa-facebook-square"></i></a></span>
    <span><a href="https://twitter.com/f1"><i class="fab fa-twitter-square"></i></a></span>
    <span><a href="https://www.youtube.com/channel/UCB_qr75-ydFVKSF9Dmo6izg"><i
          class="fab fa-youtube-square"></i></a></span>
  </div>
  </div>
  </div>
  </body>
  </html>';
      
		

    
?>