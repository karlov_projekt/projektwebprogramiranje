<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="scripts/jquery-3.7.1.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="home_page.css">
	<link rel="stylesheet" type="text/css" href="styles.css">

    <!--Fontawesome CDN-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
        integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <title>
        <?php 
            $activePage = basename($_SERVER['PHP_SELF'], ".php");
        
            switch ($activePage) {
                case "home": 
                    echo "Home";
                    break;
                case "driver_lineup": 
                    echo "Driver lineup";
                    break;
                case "previous_seasons": 
                    echo "Previous seasons";
                    break;
                case "forum": 
                    echo "Forum";
                    break;
            }
        ?>
    </title>
</head>
<body>
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item <?php if ($activePage=="home") {echo "active"; }?>">
                    <a class="nav-link" href="<?php if ($activePage=="home") {echo "#"; } else {echo "home.php";}?>">Home</a>
                </li>
                <li class="nav-item <?php if ($activePage=="driver_lineup") {echo "active"; }?>">
                    <a class="nav-link" href="<?php if ($activePage=="driver_lineup") {echo "#"; } else {echo "driver_lineup.php";}?>">Driver Standings</a>
                </li>
                <li class="nav-item <?php if ($activePage=="previous_seasons") {echo "active"; }?>">
                    <a class="nav-link" href="<?php if ($activePage=="previous_seasons") {echo "#"; } else {echo "previous_seasons.php";}?>">Previous seasons</a>
                </li>
                <li class="nav-item <?php if ($activePage=="forum") {echo "active"; }?>">
                    <a class="nav-link" href="<?php if ($activePage=="forum") {echo "#"; } else {echo "forum.php";}?>">Forum</a>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="login.php">Logout</a>
                </li>
            </ul>
        </div>
    </nav>
    
    <div class="forum_content">
        <img src="assets\HD-wallpaper-f1-2022-w1-cars-motorsport-f1-car-formula-1.jpg" class="banner_img">
        <div class="content">
